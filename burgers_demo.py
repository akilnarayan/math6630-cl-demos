# Uses different finite-volume-type methods to solve scalar Burgers'
# equation with periodic boundary conditions.

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as animation

animate = True

# Define PDE:
#
#  u_t + f(u)_x = 0
#  u(x,0) = u0(x)
#  + periodic boundary conditions on [-1,1]

f = lambda u: u**2/2.
fprime = lambda u: u    # We'll need this for upwinding

u0 = lambda x: (np.exp(np.sin(np.pi*x)) - np.exp(-1.))/(np.exp(1.) - np.exp(-1.))
#u0 = lambda x: 1.*(np.abs(x) < 0.5)
#u0 = lambda x: 1.*(np.abs(x) < 0.5) - 1*(np.abs(x) >= 0.5)

# Terminal time
T = 1.5
# Number of spatial grid points
N = 256
h = 2./N
# CFL condition
dt = h/2.
lmbda = dt/h

# Temp stuff
inds = np.arange(N)

# Set up initial condition: to second order, u0(x) at cell centers is
# equivalent to cell average
xc = np.linspace(-1., 1., N+1)[:-1] + h/2.
u_fd = u0(xc)
u_fd2 = u0(xc)
u_roe = u0(xc)
u_lf = u0(xc)

t = 0.

if animate:
    Nsteps = int(np.ceil(T/dt))
    Ufd = np.zeros((N, 1 + Nsteps))
    Ufd2 = np.zeros((N, 1 + Nsteps))
    Uroe = np.zeros((N, 1 + Nsteps))
    Ulf = np.zeros((N, 1 + Nsteps))
    Ufd[:,0] = u_fd
    Uroe[:,0] = u_roe
    Ulf[:,0] = u_lf
    step = 0

# The following functions define the schemes. Each returns a vector rhs
# for the scheme
#
#    u_j^{n+1} = u_j^n - lmbda * rhs

def fd_scheme1(uu,tt):
    # Corresponds to the scheme
    #
    # u_j^{n+1} = u_j^n - lmbda * f'(u_j^n) ( u_j^n - u_{j-1}^n ),    OR
    # u_j^{n+1} = u_j^n - lmbda * f'(u_j^n) ( u_{j+1}^n - u_j^n ),
    #
    # depending on upwind velocity u

    rhs = np.zeros(uu.shape)

    # Upwind flags: "velocity" is u
    fp = fprime(uu)
    left_flags = fp >= 0.
    rhs[left_flags] = fp[left_flags] * ( uu[left_flags] - uu[(inds[left_flags]-1)%N] )
    rhs[~left_flags] = fp[~left_flags] * ( uu[(inds[~left_flags]+1)%N] - uu[~left_flags] )

    return rhs

def fd_scheme2(uu,tt):
    # Corresponds to the scheme
    #
    # u_j^{n+1} = u_j^n - lmbda * ( f(u_j^n) - f(u_{j-1}^n) ),    OR
    # u_j^{n+1} = u_j^n - lmbda * ( f(u_{j+1}^n) - f(u_j^n) ),
    #
    # depending on upwind velocity u

    rhs = np.zeros(uu.shape)

    # Upwind flags: "velocity" is u
    fp = fprime(uu)
    left_flags = fp >= 0.
    rhs[left_flags] =  ( f(uu[left_flags]) - f(uu[(inds[left_flags]-1)%N]) )
    rhs[~left_flags] = ( f(uu[(inds[~left_flags]+1)%N]) - f(uu[~left_flags]) )

    return rhs

def roe_scheme(uu,tt):
    # Corresponds to the scheme
    #   u_j^{n+1} = u_j^n - lmbda * ( F_{j+} - F_{j-} )
    # where F is the Roe flux

    fu = f(uu)

    # Compute Roe flux F at all cell interfaces
    # F[j] --> flux between u[j-1] and u[j]
    s = (fu - fu[(inds-1)%N])/(uu - uu[(inds-1)%N])
    s[np.isnan(s)] = 0. # nan fix

    F = np.zeros(uu.shape)
    F[0] = fu[-1] if s[0]>=0 else fu[0]
    F[1:] = fu[:-1]*(s[1:]>=0) + fu[1:]*(s[1:]<0)

    return F[(inds+1)%N] - F[inds]

alph = np.max(fprime(u_lf)) # An input parameter for the Lax-Friedrichs' flux
def lf_scheme(uu,tt):
    # Corresponds to the scheme
    #   u_j^{n+1} = u_j^n - lmbda * ( F_{j+} - F_{j-} )
    # where F is the Lax-Friedrichs flux

    # Compute Lax-Friedrichs flux F at all cell interfaces
    # F[j] --> flux between u[j-1] and u[j]
    fu = f(uu)
    F = 1/2. * ( fu + fu[(inds-1)%N] ) - alph/2. * ( uu - uu[(inds-1)%N] )

    return F[(inds+1)%N] - F[inds]

while t < T - 1e-12:

    if T-t < dt:
        dt = (T-t)
        lmbda = dt/h

    # Forward Euler
    u_fd  -= lmbda * fd_scheme1(u_fd,  t)
    u_fd2  -= lmbda * fd_scheme2(u_fd2,  t)
    u_roe -= lmbda * roe_scheme(u_roe, t)
    u_lf  -= lmbda * lf_scheme(u_lf,  t)

    t += dt

    if animate:
        Ufd[:,step+1] = u_fd
        Ufd2[:,step+1] = u_fd2
        Uroe[:,step+1] = u_roe
        Ulf[:,step+1] = u_lf
        step += 1

if animate:
    fig = plt.figure()
    ax = plt.subplot(2,4,1)
    fd1_line = plt.plot([], [], 'r')[0]
    ax.set_xlim(-1, 1)
    spread = np.max(u0(xc)) - np.min(u0(xc))
    ax.set_ylim(-0.1*spread + np.min(u0(xc)), 0.1*spread + np.max(u0(xc)))
    time_template = 'time = %.3fs'
    time_text = ax.text(0.05, 0.90, '', transform=ax.transAxes)
    ax.set_title('FD scheme 1')

    ax = plt.subplot(2,4,2)
    fd2_line = plt.plot([], [], 'r')[0]
    ax.set_xlim(-1, 1)
    spread = np.max(u0(xc)) - np.min(u0(xc))
    ax.set_ylim(-0.1*spread + np.min(u0(xc)), 0.1*spread + np.max(u0(xc)))
    ax.set_title('FD scheme 2')

    ax = plt.subplot(2,4,3)
    roe_line = plt.plot([], [], 'r')[0]
    ax.set_xlim(-1, 1)
    spread = np.max(u0(xc)) - np.min(u0(xc))
    ax.set_ylim(-0.1*spread + np.min(u0(xc)), 0.1*spread + np.max(u0(xc)))
    ax.set_title('FV Roe scheme')

    ax = plt.subplot(2,4,4)
    lf_line = plt.plot([], [], 'r')[0]
    ax.set_xlim(-1, 1)
    spread = np.max(u0(xc)) - np.min(u0(xc))
    ax.set_ylim(-0.1*spread + np.min(u0(xc)), 0.1*spread + np.max(u0(xc)))
    ax.set_title('FV Lax-Friedrichs scheme')

    plt.subplot(2,4,5)
    plt.plot(xc, u0(xc), 'k--', xc, u_fd, 'r')

    plt.subplot(2,4,6)
    plt.plot(xc, u0(xc), 'k--', xc, u_fd2, 'r')

    plt.subplot(2,4,7)
    plt.plot(xc, u0(xc), 'k--', xc, u_roe, 'r')

    plt.subplot(2,4,8)
    plt.plot(xc, u0(xc), 'k--', xc, u_lf, 'r')

    def animation_init():
        time_text.set_text('')
        return fd1_line, fd2_line, roe_line, lf_line, time_text

    def animation_update(i):
        fd1_line.set_data(xc, Ufd[:,i])
        fd2_line.set_data(xc, Ufd2[:,i])
        roe_line.set_data(xc, Uroe[:,i])
        lf_line.set_data(xc, Ulf[:,i])
        time_text.set_text(time_template % (i*dt))
        return fd1_line, fd2_line, roe_line, lf_line, time_text

    ani = animation.FuncAnimation(fig, animation_update, np.arange(0, Nsteps, 1),
                      interval=1, blit=True, init_func=animation_init, repeat_delay=300)

    plt.show()
