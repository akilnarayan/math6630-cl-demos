# Uses different finite-volume-type methods to solve scalar Burgers'
# equation with periodic boundary conditions.
# The chosen numerical flux is a second-order accurate ("non-limited")
# flux, and introduces spurious oscillations.

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as animation

animate = True

# Define PDE:
#
#  u_t + f(u)_x = 0
#  u(x,0) = u0(x)
#  + periodic boundary conditions on [-1,1]

f = lambda u: u**2/2. # Actually not used anywhere

u0 = lambda x: -np.sin(np.pi*x)

# Terminal time
T = 1.5
# Number of spatial grid points
N = 128
h = 2./N
# CFL condition
dt = h/2.
lmbda = dt/h

# Temp stuff
inds = np.arange(N)

# Set up initial condition: to second order, u0(x) at cell centers is
# equivalent to cell average
xc = np.linspace(-1., 1., N+1)[:-1] + h/2.
u_fv2 = u0(xc)

t = 0.

if animate:
    Nsteps = int(np.ceil(T/dt))
    U = np.zeros((N, 1 + Nsteps))
    U[:,0] = u_fv2
    step = 0

# The following functions define the schemes. Each returns a vector rhs
# for the scheme
#
#    u_j^{n+1} = u_j^n - lmbda * rhs

def fv_secondorder(uu,tt):
    # Corresponds to the scheme
    #
    # u_j^{n+1} = u_j^n - lmbda * ( f_{j+} - f_{j-} ),
    #
    # specialized for Burgers' equation where
    #
    # f_{j+} = 1/6 ( (u_{j+1})^2 + u_j^2 + u_j u_{j+1} )
    # f_{j-} = 1/6 ( (u_{j-1})^2 + u_j^2 + u_j u_{j-1} )

    rhs = np.zeros(uu.shape)
    rhs = uu[(inds+1)%N]**2 - uu[(inds-1)%N]**2 + uu*uu[(inds+1)%N] - uu*uu[(inds-1)%N]

    return rhs/6.

while t < T - 1e-12:

    if T-t < dt:
        dt = (T-t)
        lmbda = dt/h

    # Forward Euler
    u_fv2 -= lmbda * fv_secondorder(u_fv2, t)

    t += dt

    if animate:
        U[:,step+1] = u_fv2
        step += 1

if animate:
    fig = plt.figure()

    ax = plt.subplot(2,1,1)
    fv2_line = plt.plot([], [], 'r')[0]
    ax.set_xlim(-1, 1)
    spread = np.max(u0(xc)) - np.min(u0(xc))
    ax.set_ylim(-0.1*spread + np.min(u0(xc)), 0.1*spread + np.max(u0(xc)))
    time_template = 'time = %.3fs'
    time_text = ax.text(0.05, 0.90, '', transform=ax.transAxes)
    ax.set_title('FV scheme')

    plt.subplot(2,1,2)
    ax = plt.subplot(2,1,2)
    ax.set_xlim(-1, 1)
    ax.plot(xc, u0(xc), 'k--', label='Initial data')
    ax.plot(xc, u_fv2, 'r', label='Computed solution')
    ax.legend()

    def animation_init():
        time_text.set_text('')
        return fv2_line, time_text

    def animation_update(i):
        fv2_line.set_data(xc, U[:,i])
        time_text.set_text(time_template % (i*dt))
        return fv2_line, time_text

    ani = animation.FuncAnimation(fig, animation_update, np.arange(0, Nsteps, 1),
                      interval=1, blit=True, init_func=animation_init, repeat_delay=300)

    plt.show()
